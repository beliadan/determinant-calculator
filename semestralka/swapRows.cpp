#include "swapRows.h"


vector<vector<double>> swapRows(vector<vector<double>>& matrix, int row1, int row2) {
    for (int i = 0; i < matrix[0].size(); ++i) {
        swap(matrix[row1][i], matrix[row2][i]);
    }
    return matrix;
}