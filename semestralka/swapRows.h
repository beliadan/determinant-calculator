#ifndef SEMESTRALKA__SWAPROWS_H
#define SEMESTRALKA__SWAPROWS_H
#include <vector>
using namespace std;
vector<vector<double>> swapRows(vector<vector<double>>& matrix, int row1, int row2);
#endif
