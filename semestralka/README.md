## Introduction
Hello, Dear User,

this is my C++ project "Calculating determinant of matrix using GEM or LU".

## Description of project. 
In this program are implemented two algorithms for calculating determinant of matrix. 
There were implemented and compared two algorithms:
- Gaussian Elimination Method
- LU decomposition

## Description of implementation
### LU Decomposition:
LU decomposition is a method for factorizing a matrix into the product of a lower triangular matrix (L) and an upper triangular matrix (U). 
The algorithm is implemented as follows:

1. Create a copy of the input matrix to avoid modifying the original matrix.
2. Initialize matrices L and U with appropriate sizes.
3. Iterate through each column of the matrix.
4. Check and handle the case where the pivot element is zero.
5. Calculate the elements of the lower triangular matrix (L) and update the matrix accordingly.
6. Compute the determinant by multiplying the absolute values of the diagonal elements of the updated matrix.

### Gaussian Elimination:
Gaussian Elimination is a method for transforming a matrix into row-echelon form. 
The algorithm is implemented as follows:

1. Create a copy of the input matrix to avoid modifying the original matrix.
2. Iterate through each column of the matrix.
3. Check and handle the case where the pivot element is zero.
4. Update the matrix using Gaussian Elimination to achieve row-echelon form.
5. Multiply the pivot elements at each step to obtain the determinant.


## Instruction of usage.
Using this program zou can calculate determinant of entered or random matrix.
If you want to calculate determinant of your own matrix, 
please add .txt file of your matrix to "matrices" folder before running program.
After that you can run the program.
```
./semestralka_ {--help |--read <matrix_filename.txt> | --rand <size>} {--gem|--lu}
```
## Example of adding your own matrix
In the first line, enter the number of rows and columns. Note that the GEM and LU work only with square matrices, so the number of rows and columns should be the same. Then, line by line, enter the values of the matrix.
```
0 1 2
1 0 3
2 3 0
```

## Performance Comparison
For performance comparison was used operational system "macOS Monterey 12.5 with processor 2,3 GHz Dual-Core Intel Core i5.

This table provides a performance comparison between GEM and LU for matrix operations across different matrix sizes. 
The times are presented in milliseconds (ms).

| Case № | SIZE      | GEM        | LU         |
|--------|-----------|------------|------------|
| **1**  | 3x3       | < 1 ms     | < 1 ms     |
| **2**  | 5x5       | < 1 ms     | < 1 ms     |
| **3**  | 10x10     | < 1 ms     | < 1 ms     |
| **4**  | 50x50     | < 1 ms     | < 1 ms     |
| **5**  | 100x100   | ≈ 3 ms     | ≈ 5 ms     |
| **6**  | 250x250   | ≈ 50 ms    | ≈ 70 ms    |
| **7**  | 500x500   | ≈ 360 ms   | ≈ 520 ms   |
| **8**  | 750x750   | ≈ 1190 ms  | ≈ 1730 ms  |
| **9**  | 1000x1000 | ≈ 2800 ms  | ≈ 4100 ms  |
| **10** | 2000x2000 | ≈ 22300 ms | ≈ 32700 ms |

## Performance Comparison Conclusion

- Consistency for Small Matrices: Both GEM and LU perform consistently fast for small matrices (3x3 to 10x10), taking less than 1 millisecond.
- Increasing Execution Times: Execution times increase with matrix size, reflecting the expected computational complexity for larger matrices.
- GEM vs. LU Performance: Across all sizes, LU generally takes more time than GEM. GEM exhibits better performance, especially for larger matrices (750x750, 1000x1000, 2000x2000).
- Significant Increase for Large Matrices: Both algorithms experience a significant increase in execution time for the largest matrix (2000x2000), highlighting the computational challenges of handling large matrices.
- Approximations: Times are approximations, subject to variation based on implementation, hardware, and optimization.
- Consideration for Application: The choice between GEM and LU should consider specific application requirements, with LU showing better performance for larger matrices in the tested scenarios.

![chart](chart.png)













