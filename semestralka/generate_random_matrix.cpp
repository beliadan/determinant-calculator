#include "generate_random_matrix.h"
#include <vector>
#include <random>

int get_random_number() {
    static std::mt19937 mt{ std::random_device{}() };
    static std::uniform_int_distribution<> dist(-1, 1);
    return dist(mt);
}

vector<vector<double>> generate_random_matrix(int size) {
    vector<vector<double>> matrix(size, vector<double>(size));

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            double randomNum = get_random_number();
            matrix[i][j] = randomNum;
        }
    }
    return matrix;
}