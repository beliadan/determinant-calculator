#ifndef SEMESTRALKA__ALGORITHMEXCEPTION_H
#define SEMESTRALKA__ALGORITHMEXCEPTION_H

#include <iostream>
#include <stdexcept>
#include <utility>

class AlgorithmException : public std::exception {
private:
    std::string errorMessage;

public:

    explicit AlgorithmException(std::string  message);

    [[nodiscard]] const char* what() const noexcept override;
};


#endif
