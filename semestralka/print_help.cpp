#include "print_help.h"
using namespace std;
#include <iostream>

void print_help(){
    cout << "Usage: ./semestralka_ {--help |--read <matrix_filename.txt> | --rand <size>} {--gem|--lu}" << endl;
    cout << "Options:" << endl;
    cout << "  --help Show this help message and exit" << endl;
    cout << "  --read <matrix_filename.txt>  Read matrix from file" << endl;
    cout << "  --rand <size> Generate random matrix with size <size>" << endl;
    cout << "  --gem Use Gaussian Elimination Method" << endl;
    cout << "  --lu Use LU Decomposition" << endl;
}
