#include "enter_matrix.h"
#include <filesystem>
#include "readMatrixFromFile.h"
using namespace std;
namespace fs = filesystem;

bool input_is_correct(const string& matrix_filename) {
    return (fs::exists(fs::current_path().parent_path() / "matrices" / matrix_filename));
}

vector<vector<double>> enter_matrix_filename(string matrix_filename){
    if (!input_is_correct(matrix_filename)) {
        cerr << endl << "There is no file with name '" + matrix_filename + "'" << endl;
        exit(0);
    }

    vector<vector<double>> matrix = readMatrixFromFile(matrix_filename);

    if (matrix[0].size()!=matrix.size()) {
        cerr << "Matrix must be square" << endl;
        exit(0);
    }
    return matrix;
}


