#include "print_welcome_text.h"
#include <iostream>
using namespace std;

void print_welcome_text(){
    cout << "Hello, dear user!" << endl
         << "Due to this program you can find a determinant of matrix." << endl
         << " " << endl;
}
