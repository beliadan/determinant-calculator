#include <iostream>
#include <vector>
#include "print_welcome_text.h"
#include "enter_matrix.h"
#include "calculate_determinant.h"
#include "print_help.h"
#include "generate_random_matrix.h"
#include "lu.h"
#include "gem.h"
using namespace std;


int main(int argc, char* argv[]) {
    string algorithm_name;
    vector<vector<double>> matrix;
    print_welcome_text();

    if (argv[1] == string("--help")) {
        print_help();
        return 0;
    }
    if (argc != 4) {
        cerr << "Not enough or too much arguments" << endl;
        print_help();
        return 0;
    }
    if (argv[1] == string("--read")){
        matrix = enter_matrix_filename(argv[2]);
    } else if (argv[1] == string("--rand")) {
        if (stoi(argv[2]) <= 0){
            cerr << "Incorrect size of matrix" << endl;
            print_help();
            return 0;
        }
        matrix = generate_random_matrix(stoi(argv[2]));
    }
    if (argv[3] == string("--gem") || argv[3] == string("--lu")) {
        calculate_determinant(matrix, string(argv[3]));
    } else {
        cerr << "There is no command: " << string(argv[3]) << endl;
        print_help();
        return 0;
    }

    return 0;
}
