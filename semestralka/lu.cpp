#include "lu.h"
#include "readMatrixFromFile.h"
#include <vector>
#include "swapRows.h"
#include "AlgorithmException.h"
using namespace std;

double determinantLU(const vector<vector<double>>& matrix) {
    size_t size = matrix.size();
    vector<vector<double>> matrix_copy(matrix);

    vector<vector<double>> L(size, vector<double>(size));
    vector<vector<double>> U(size, vector<double>(size));

    for (int i = 0; i < size; i++) {
        for (int j = i + 1; j < size; j++) {
            double pivot = abs(matrix_copy[i][i]);
            if (pivot == 0) {
                for (int f = 0; f < size; ++f) {
                    if (matrix_copy[f][i] != 0) {
                        matrix_copy = swapRows(matrix_copy, i, f);
                        pivot = matrix_copy[i][i];
                        break;
                    }
                }
            }
            L[j][i] = matrix_copy[j][i] / pivot;

            for (int k = i + 1; k < size; k++) {
                matrix_copy[j][k] -= L[j][i] * matrix_copy[i][k];
            }
        }
    }
    double det = 1;
    for (int i = 0; i < size; i++) {
        det *= abs(matrix_copy[i][i]);
    }

    if (!isinf(det)) {
        return det;
    } else {
        throw AlgorithmException("It is not possible to obtain an exact value, result exceeded the maximum value of double. Matrix is too big");
    }
}

void lu(const vector<vector<double>>& matrix){
    try {
        double det = determinantLU(matrix);
        cout << "Determinant of matrix is: " << det << endl
             << " " << endl;
    } catch (const AlgorithmException& e) {
        cout << "It is not possible to obtain an exact value, result exceeded the maximum value of double. Matrix is too big" << endl;
    }

}


