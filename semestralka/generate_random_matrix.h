#ifndef SEMESTRALKA__GENERATE_RANDOM_MATRIX_H
#define SEMESTRALKA__GENERATE_RANDOM_MATRIX_H
using namespace std;
#include <vector>

vector<vector<double>> generate_random_matrix(int size);

#endif
