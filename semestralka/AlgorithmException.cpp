//
// Created by Даниил Беляев on 26.12.2023.
//

#include "AlgorithmException.h"



AlgorithmException::AlgorithmException(std::string  message) : errorMessage(std::move(message)) {}

const char* AlgorithmException::what() const noexcept {
    return errorMessage.c_str();
}