#include "calculate_determinant.h"
#include "lu.h"
#include "gem.h"

template <typename TimePoint>
chrono::milliseconds to_ms(TimePoint tp) {
    return chrono::duration_cast<std::chrono::milliseconds>(tp);
}

void calculate_determinant(vector<vector<double>>& matrix,const string& alg){
    auto start = chrono::high_resolution_clock::now();
    if (alg=="--gem") {
        gem(matrix);
    } else if (alg=="--lu"){
        lu(matrix);
    }
    auto end = std::chrono::high_resolution_clock::now();
    cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";
}
