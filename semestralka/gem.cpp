#include "gem.h"
#include <vector>
#include <cmath>
#include "readMatrixFromFile.h"
#include "swapRows.h"
using namespace std;
#include "AlgorithmException.h"

double determinantGEM(const vector<vector<double>>& matrix) {
    size_t size = matrix.size();
    vector<vector<double>> matrix_copy(matrix);
    double det = 1;
    for (int k = 0; k < size; ++k) {
        double pivot = abs(matrix_copy[k][k]);
        if (pivot == 0) {
            for (int i = k + 1; i < size; ++i) {
                if (matrix_copy[i][k] != 0) {
                    matrix_copy = swapRows(matrix_copy, k, i);
                    pivot = matrix_copy[k][k];
                    break;
                }
            }
        }
        for (int i = k + 1; i < size; ++i) {
            double factor = matrix_copy[i][k] / pivot;
            if (!isnan(factor)) {
                for (int j = k; j < size; ++j) {
                    matrix_copy[i][j] -= factor * matrix_copy[k][j];
                }
            }
        }
        det *= pivot;
    }
    if (!isinf(det)) {
        return det;
    } else {
        throw AlgorithmException("It is not possible to obtain an exact value, result exceeded the maximum value of double. Matrix is too big");
    }
}


void gem(const vector<vector<double>>& matrix){
    try {
        double det = determinantGEM(matrix);
        cout << "Determinant of matrix is: " << det << endl
             << " " << endl;
    } catch (const AlgorithmException& e) {
        cout << "It is not possible to obtain an exact value, result exceeded the maximum value of double. Matrix is too big" << endl;
    }
}