#include "readMatrixFromFile.h"
#include <fstream>
#include <iostream>
namespace fs = filesystem;

vector<vector<double>> readMatrixFromFile(const string& filename) {
    vector<vector<double>> matrix;
    fs::path current_directory = fs::current_path().parent_path();
    ifstream file(current_directory / "matrices" / filename);
    if (file.is_open()) {
        double value;
        while (file >> value) {
            matrix.emplace_back(1, value);
            while (file.peek() == ' ') {
                if (file.peek() == '\n') {
                    break;
                }
                file.ignore();
                file >> value;
                matrix.back().emplace_back(value);
            }
        }
        file.close();
    } else {
        cerr << "Unable to open file: " << filename << endl;
        exit(0);
    }


    return matrix;
}
