#include <iostream>
using namespace std;
#ifndef SEMESTRALKA__CALCULATE_DETERMINANT_H
#define SEMESTRALKA__CALCULATE_DETERMINANT_H

void calculate_determinant(vector<vector<double>>& matrix,const string& alg);

#endif
