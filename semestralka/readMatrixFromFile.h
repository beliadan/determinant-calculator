#include <vector>
#include <fstream>
#include <cmath>
using namespace std;

#ifndef SEMESTRALKA__READMATRIXFROMFILE_H
#define SEMESTRALKA__READMATRIXFROMFILE_H

vector<vector<double>> readMatrixFromFile(const string& filename);

#endif
